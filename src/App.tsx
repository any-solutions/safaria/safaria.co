import ContactUs from "./components/ContactUs";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Hero from "./components/Hero";
import HowDoesItWork from "./components/HowDoesItWork";
import WhatIsSafaria from "./components/WhatIsSafaria";
import ScrollToHashElement from "./components/ScrollToHashElement";

function App() {
  return (
    <>
      <ScrollToHashElement />
      <Header />
      <Hero />
      <WhatIsSafaria />
      <HowDoesItWork />
      <ContactUs />
      <Footer />
    </>
  );
}

export default App;
