import useLanguage from "./useLanguage";
import content from "../content.json";
import { useEffect, useState } from "react";

function getContentInLanguage(language: string) {
  return Object.keys(content).reduce((acc, key) => {
    // @ts-expect-error - I know that content[key] is an object
    acc[key] = content[key][language];
    return acc;
  }, {}) as { [key in keyof typeof content]: string };
}

const useContent = () => {
  const { language } = useLanguage();

  const [content, setContent] = useState(() => getContentInLanguage(language));

  useEffect(() => {
    setContent(getContentInLanguage(language));
  }, [language]);

  return content;
};

export default useContent;
