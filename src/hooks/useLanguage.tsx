import { useEffect, useState } from "react";

export type Language = "en" | "ar";

const useLanguage = () => {
  const LS_KEY = "language";
  const [language, setLanguage] = useState(
    (localStorage.getItem(LS_KEY) as Language) || "en"
  );

  useEffect(() => {
    document.documentElement.dir = language === "ar" ? "rtl" : "ltr";
    document.documentElement.lang = language;
  }, []);

  const toggleLanguage = () => {
    const newLanguage = language === "en" ? "ar" : "en";
    localStorage.setItem(LS_KEY, newLanguage);
    setLanguage(newLanguage);
    
    // reload the page to apply the new language
    // TODO: remove this when language is applied using a shared context and can be updated without a page reload.
    window.location.reload();
  };

  return { language, toggleLanguage };
};

export default useLanguage;
