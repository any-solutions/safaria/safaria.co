import { useNavigate } from "react-router-dom";
import SafariaLogo from "./components/SafariaLogo";

const _404 = () => {
  const navigate = useNavigate();
  return (
    <div className="flex flex-col justify-center items-center gap-4 p-8 text-center">
      <div className="absolute top-0 left-0 w-screen h-screen bg-gradient-to-b from-white from-50% to-primary text-center px-4 -z-10" />
      <SafariaLogo />
      <h1 className="text-3xl mt-6">404 Not Found</h1>
      <p>Sorry, the page you are looking for does not exist.</p>
      <button onClick={() => navigate(-1)}>Go Back</button>
    </div>
  );
};

export default _404;
