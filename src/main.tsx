import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import _404 from "./404.tsx";
import "modern-normalize/modern-normalize.css";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import ROUTES from "./routes.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  ...ROUTES.map((route) => ({
    path: route.path,
    element: <App />,
  })),
  {
    path: "*",
    element: <_404 />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
