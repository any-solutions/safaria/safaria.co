import SafariaLogo from "./SafariaLogo";
import { useMediaQuery } from "react-responsive";
import { LanguageButton } from "./LanguageButton";
import FullscreenNav from "./FullscreenNav";
import { useState } from "react";
import { BriefcaseButton } from "./BriefcaseButton";
import useContent from "../hooks/useContent";
import RoutesListItems from "./RoutesListItems";

const Header = () => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const content = useContent();

  const smallScreen = useMediaQuery({
    query: "(max-width: 1000px)",
  });

  return (
    <header
      id="home"
      className="flex items-center justify-between w-full px-8 md:px-24 z-20 absolute top-9"
    >
      <SafariaLogo dark={!isSidebarOpen} />
      {smallScreen ? (
        <>
          <BriefcaseButton
            open={isSidebarOpen}
            onClick={() => setIsSidebarOpen(!isSidebarOpen)}
          />
          <FullscreenNav open={isSidebarOpen} setOpen={setIsSidebarOpen} />
        </>
      ) : (
        <nav className="flex items-center justify-around gap-6">
          <ul className="flex space-x-4 gap-4">
            <RoutesListItems />
          </ul>
          <button>{content.getTheAppTitle}</button>
          <LanguageButton />
        </nav>
      )}
    </header>
  );
};

export default Header;
