import { AiFillTwitterCircle } from "react-icons/ai";
import { BiLogoFacebookCircle } from "react-icons/bi";

const SocialButtons = ({ light = false }) => {
  return (
    <div className={`flex gap-4`}>
      <AiFillTwitterCircle fill={`${light ? "white" : "#212F5A"}`} size={32} className="secondary-button"/>
      <BiLogoFacebookCircle fill={`${light ? "white" : "#212F5A"}`} size={32} className="secondary-button"/>
    </div>
  );
};

export default SocialButtons;
