import SocialButtons from "./SocialButtons";
import RoutesListItems from "./RoutesListItems";
import safariaLogo from "../assets/safaria-logo.png";

const Footer = () => {
  return (
    <footer className="bg-lightGray w-full flex flex-col gap-6 p-6 rounded-t-[28px] sm:px-24 sm:text-xl">
      <ul className="flex space-x-4 gap-4">
        <RoutesListItems />
      </ul>
      <SocialButtons />
      <div className="self-end">
        <img src={safariaLogo} alt="Safaria Bag" width={180} />
      </div>
    </footer>
  );
};

export default Footer;
