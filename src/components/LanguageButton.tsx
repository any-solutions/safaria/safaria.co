import { VscGlobe } from "react-icons/vsc";
import useLanguage from "../hooks/useLanguage";

export const LanguageButton = () => {
  const { language, toggleLanguage } = useLanguage();

  return (
    <div className="flex items-center gap-2 secondary-button w-14" onClick={toggleLanguage}>
      <VscGlobe size={24} />
      <span>{language === "en" ? "ar" : "en"}</span>
    </div>
  );
};
