import { Link, useLocation } from "react-router-dom";
import { TRoute } from "../routes";

const NavLink = ({ route, display }: { route: TRoute; display: string }) => {
  const location = useLocation();
  return (
    <Link
      to={route.path + route.hash}
      reloadDocument={route.reloadDocument}
      className={`transition-all hover:text-primaryLight ${
        location.pathname === route.path && location.hash === route.hash
          ? "font-extrabold"
          : ""
      }`}
    >
      {display}
    </Link>
  );
};

export default NavLink;
