import useContent from "../hooks/useContent";
import phone from "../assets/phone.png";

const HowDoesItWork = () => {
  const content = useContent();

  const howDoesItWorkContent = Object.keys(content).filter((key) =>
    key.startsWith("howDoesItWork")
  );
  const titles = howDoesItWorkContent
    .filter((key) => key.match("ContentTitle"))
    .sort();
  const paragraphs = howDoesItWorkContent
    .filter((key) => !key.endsWith("Title"))
    .sort();

  return (
    <section>
      <h1>{content.howDoesItWorkTitle}</h1>
      <div className="flex flex-col-reverse items-center lg:flex-row gap-9">
        <ul className="flex flex-col gap-9">
          {titles.map((title, index) => (
            <li key={title} className="flex gap-4">
              <div className="min-w-16 min-h-16 w-16 h-16 bg-primaryLight text-center rounded-lg text-white font-bold text-3xl flex justify-center items-center">
                {index + 1}
              </div>
              <div className="flex flex-col gap-1">
                {/* @ts-expect-error - titles are filtered from content's indices */}
                <h2 className="text-xl md:text-3xl font-bold">{content[title]}</h2>
                {/* @ts-expect-error - paragraphs are filtered from content's indices */}
                <p className="md:text-xl text-gray-400">{content[paragraphs[index]]}</p>
              </div>
            </li>
          ))}
        </ul>
        <img src={phone} alt="Phone" className="w-2/3 lg:w-1/3 object-contain"/>
      </div>
    </section>
  );
};

export default HowDoesItWork;
