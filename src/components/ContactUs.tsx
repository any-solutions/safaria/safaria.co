import { MdEmail, MdPhone } from "react-icons/md";
import useContent from "../hooks/useContent";

const ContactUs = () => {
  const content = useContent();

  return (
    <section id="contact">
      <h1>{content.contactUsTitle}</h1>
      <div className="bg-lightGray rounded-lg p-5 flex flex-col gap-7 justify-center items-center drop-shadow-lg w-full md:flex-row">
        <div className="flex flex-col bg-primary rounded-lg p-5 text-white text-xl font-semibold drop-shadow-lg md:h-full">
          <a
            href="mailto:support@safaria.co"
            className="flex items-center gap-3"
          >
            <MdEmail />
            support@safaria.co
          </a>
          <a href="tel:+966500000000" className="flex items-center gap-3">
            <MdPhone />
            966 500000000
          </a>
        </div>
        <div className="flex flex-col gap-7 w-3/4 justify-around">
          <p className="text-2xl font-bold text-center px-4">
            {content.notifyMeContent}
          </p>
          <form
            className="w-full flex flex-col items-center justify-center gap-4"
            onSubmit={(e) => e.preventDefault()}
          >
            <input
              type="email"
              id="email"
              autoComplete="email"
              placeholder={content.emailPlaceholder}
              className="w-full"
            />
            <button onClick={(e) => e.preventDefault()}>
              {content.notifyMeTitle}
            </button>
          </form>
        </div>
      </div>
    </section>
  );
};

export default ContactUs;
