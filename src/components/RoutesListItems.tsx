import useContent from "../hooks/useContent";
import ROUTES, { TRoute } from "../routes";
import NavLink from "./NavLink";

const RoutesListItems = ({
  onClick,
}: {
  onClick?: (route: TRoute) => void;
}) => {
  const content = useContent();
  return ROUTES.map((route) => (
    <li key={route.path + route.hash} onClick={() => onClick && onClick(route)}>
      {/* @ts-expect-error - skip */}
      <NavLink route={route} display={content[route.contentKey]} />
    </li>
  ));
};

export default RoutesListItems;
