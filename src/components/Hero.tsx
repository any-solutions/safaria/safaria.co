import useContent from "../hooks/useContent";
import googlePlay from "../assets/google-play-badge.png";
import appStore from "../assets/app-store-badge.png";
import phones from "../assets/phones.png"

const Hero = () => {
  const content = useContent();

  return (
    <div className="max-h-screen w-full h-screen overflow-hidden flex flex-col justify-around pt-36 items-center gap-8 bg-gradient-to-b from-white from-50% to-primary text-center px-4">
      <h1 className="text-6xl">{content.safaria}</h1>
      <p className="text-3xl font-bold">{content.hero}</p>
      <div className="downloadButtons flex gap-4 flex-wrap justify-center items-center">
        <img className="secondary-button" src={googlePlay} alt="Google Play" />
        <img className="secondary-button" src={appStore} alt="App Store" />
      </div>
      <img className="phones max-h-[60vh] object-cover -mb-36" src={phones} alt="Phones" />
    </div>
  );
};

export default Hero;
