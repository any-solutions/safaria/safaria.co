export const BriefcaseButton = ({
  open,
  onClick,
}: {
  open: boolean;
  onClick?: () => void;
}) => {
  const handleClick = () => {
    onClick && onClick();
  };

  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 24 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      onClick={handleClick}
    >
      <path
        d={
          open
            ? "M9 0C7.896 0 7 0.896 7 2V4H9V2.5C9 2.224 9.224 2 9.5 2H14.5C14.776 2 15 2.224 15 2.5V4H17V2C17 0.896 16.104 0 15 0H9ZM21.561 12L20.361 18H3.64L2.44 12H21.561ZM24 10H0L2 20H22L24 10ZM23 5V8H21V7H3V8H1V5H23Z"
            : "M22 7V18H2V7H22ZM24 5H0V20H24V5ZM9 0C7.896 0 7 0.896 7 2V4H9V2.5C9 2.224 9.224 2 9.5 2H14.5C14.776 2 15 2.224 15 2.5V4H17V2C17 0.896 16.104 0 15 0H9Z"
        }
        className={`${open ? "fill-white" : "fill-primary"} transition-all`}
      />
    </svg>
  );
};
