import { LanguageButton } from "./LanguageButton";
import { useEffect } from "react";
import SocialButtons from "./SocialButtons";
import useContent from "../hooks/useContent";
import RoutesListItems from "./RoutesListItems";

const FullscreenNav = ({
  open = false,
  setOpen,
}: {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const content = useContent();

  const handleToggle = () => setOpen && setOpen((prev) => !prev);

  useEffect(() => {
    if (open) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [open]);

  return (
    <nav
      className={`flex flex-col justify-center items-center w-screen h-screen -z-10 bg-primary text-white fixed bottom-0 left-0 gap-10 transition-all ${
        open ? "opacity-100" : "opacity-0 pointer-events-none"
      } select-none`}
    >
      <ul className="flex flex-col items-center justify-center space-y-6">
        <RoutesListItems onClick={handleToggle}/>
      </ul>
      <button className="bg-white text-black hover:text-white">
        {content.getTheAppTitle}
      </button>
      <LanguageButton />
      <div className="fixed bottom-16">
        <SocialButtons light />
      </div>
    </nav>
  );
};

export default FullscreenNav;
