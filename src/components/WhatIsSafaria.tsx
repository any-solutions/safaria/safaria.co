import useContent from "../hooks/useContent";
import safariaLogo from "../assets/safaria-logo.png"
import { useMediaQuery } from "react-responsive";

const WhatIsSafaria = () => {
  const content = useContent();

  const shouldShowImage = useMediaQuery({ query: "(min-width: 1000px)" });

  return (
    <section id="about">
      <h1>{content.whatIsSafariaTitle}</h1>
      <div className="flex gap-32 items-center">
        <p className="text-2xl">{content.whatIsSafariaContent}</p>
        {/* change image size */}
        {shouldShowImage && <img src={safariaLogo} alt="Safaria Bag" width={350} />}
      </div>
    </section>
  );
};

export default WhatIsSafaria;
