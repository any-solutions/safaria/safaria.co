const ROUTES: TRoute[] = [
  {
    path: "/",
    name: "Home",
    hash: "",
    contentKey: "navHome",
  },
  {
    path: "/",
    name: "About",
    hash: "#about",
    contentKey: "navAbout",
  },
  {
    path: "/",
    name: "Contact",
    hash: "#contact",
    contentKey: "navContact",
  },
  {
    path: "/privacy_policy.html",
    name: "Privacy Policy",
    hash: "",
    contentKey: "navPrivacyPolicy",
    reloadDocument: true,
  }
];

export type TRoute = {
  path: string;
  name: string;
  hash: string;
  contentKey: string;
  reloadDocument?: boolean;
};

export default ROUTES;