/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        display: ["Poppins", "ui-serif"],
      },
      colors: {
        primary: "#212F5A",
        primaryLight: "#618BCA",
        black: "#191919",
        white: "#FFFFFF",
        lightGray: "#F0F0F0",
      },
      borderRadius: {
        "t-4xl": "2rem",
        "t-5xl": "2.5rem",
        "t-6xl": "3rem",
      },
    },
  },
  plugins: [],
};
